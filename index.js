//3

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'GET'
})
.then(response => response.json())
.then(data => {
	console.log(data)
})

//4

fetch('https://jsonplaceholder.typicode.com/todos',{
	method: 'GET'
})
.then(response => response.json()) 
.then(data =>{
	let array = data.map(function(title){
		return `${title.title}`
	})

	console.log(array)
}) 

//5

fetch('https://jsonplaceholder.typicode.com/todos/5', {
	method: 'GET'
})
.then(response => response.json())
.then(data => {
	console.log(data)
//6
 	console.log(data.title, data.completed)
})

//7 

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'making activity',
		completed: false,
		userID: 1
	})
})
.then(response => response.json())
.then(json => console.log(json))

//8

fetch('https://jsonplaceholder.typicode.com/todos/5', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		id: 5,
		title: "this is updated",
		completed: true,
		userID: 1
	})
})
.then(res => res.json())
.then(data => console.log(data))

//9

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers:{
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'New Todo',
		description: "new todo list",
		status: "ongoing",
		dateCompleted: "eta: tom",
		userId: 2,
	})
}).then(response => response.json()) 
.then(data =>{
	console.log(data)
}) 

//10

fetch('https://jsonplaceholder.typicode.com/todos/2', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
//11
	body: JSON.stringify({
		completed: true,
		dateCompleted: "today"

	})
})
.then(res => res.json())
.then(data => console.log(data))


//12


fetch('https://jsonplaceholder.typicode.com/todos/10',{
	method: 'DELETE'
})

.then(res => res.json())
.then(data => console.log(data))






















